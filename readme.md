# EHentai Crawler

[![Bash Shell](https://badges.frapsoft.com/bash/v1/bash.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

> A simple script to index and download EHentai Pics.

EHentai Crawler is a set of shell script to download galleries from [E-Hentai](https://e-hentai.org).

## Table of Contents

- [Install](#install)
    - [Requirements](#Requirements)
    - [Downloading](#Downloading)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)


## Install

### Requirements

- Currently, EHentai Crawler only supports Linux system

- Sufficient CPU and Disk.
    - Analysing pages will increase CPU useage intensively. Make sure you are not running on a very old machine.
    - Gallery indexes typically require 50-100 bytes per gallery.

- You need to have `grep`, `curl` available (which are available on almost any Linux distribution)

### Downloading

You need to clone this repo in order to execute the scipt.

## Usage

You need to edit setting.cfg before running.

After editing, please run `bash ehentai.sh`

## Maintainers

[@void_life](https://gitlab.com/void_life).

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/cnhentai/ehentai-crawler/issues/new) or submit PRs.

## License

[GNU General Public License v3.0](LICENSE) © Void Life @ gitlab
