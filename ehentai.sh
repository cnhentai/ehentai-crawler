#!/bin/bash
echo "Ehentai Crawler V6"
source setting.cfg
echo "Possible Mode:"
echo "index, download"
read -p "Please choose one:" mode
case $mode in
    index) (
        if [ ! -d ${index_directory} ]
        then
            mkdir ${index_directory}
        fi
        tmp_dir=$(mktemp -d)
        mkdir "${tmp_dir}/status"
        mkdir "${tmp_dir}/searchpage"
        mkdir "${tmp_dir}/lasttitle"
        mkdir "${tmp_dir}/filename"
        seq ${spage} ${epage} | xargs -n 1 -P ${thread_number} bash ./index.sh ${tmp_dir}
        wait
        rm -r "${tmp_dir}"
    )
    ;;
    download) (
        if [ ! -d ${index_directory} ]
        then
            echo "Fatal: ${index_directory} is empty."
            echo "Please index first."
            exit
        fi
        if [ ! -d ${download_directory} ]
        then
            mkdir ${download_directory}
        fi
        if [ ! -f ${download_db} ]
        then
            touch ${download_db}
        fi
        tmp_dir=$(mktemp -d)
        mkdir "${tmp_dir}/status"
        mkdir "${tmp_dir}/galleryentry"
        mkdir "${tmp_dir}/saved_comic"
        mkdir "${tmp_dir}/bigpics"
        echo "Total Index : $(ls -1 ${index_directory} | wc -l)"
        find ${index_directory}/ -name "*.index" | xargs -r -n 1 -P ${thread_number} bash ./download.sh ${tmp_dir}
        wait
        rm -r "${tmp_dir}"
    )
    ;;
esac
exit