#!/bin/bash
source setting.cfg
if [ -f "${1}/stop.eh" ]
then
    warn_banned="false"
else
    warn_banned="true"
fi
url="https://e-hentai.org/?page=${2}${search_string}"
curl --retry 64 --retry-connrefused --tcp-fastopen -s "${url}" >"${1}/searchpage/${2}.tmp"
if [ ! -s "${1}/searchpage/${2}.tmp" ]
then
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            touch "${1}/stop.eh"
            echo "Unable to download page ${2}. You might have been banned."
        fi
    fi
    exit
fi
grep "Your IP address has been temporarily banned for" "${1}/searchpage/${2}.tmp" && (
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            touch "${1}/stop.eh"
            echo "Unable to download page ${2}. Your have been banned."
            curl --tcp-fastopen -s "${target}"  >>${log}
        fi
    fi
    exit
)
echo "Downloaded Page ${2}..."
cat "${1}/searchpage/${2}.tmp" | while read line
do
    if [ "${line}" != "" ]
    then
        if [ "${line##*No?unfiltered?results?in?this?page?range??You?either?requested?an?invalid?page?or?used?too?aggressive?filters*}" == "" ]
        then
            exit
        else
            if [ "${line##*Showing*results*}" == "" ]
            then
                echo "${line}" |xargs -d '<' -n 1 | while read line_2
                do
                    if [ "${line_2}" != "" ]
                    then
                        if [ "${line_2##img?style*alt*title*}" == "" ]
                        then
                            title_raw="${line_2#img?style*alt*title??}"
                            title="${title_raw%%??src*}"
                            unset title_raw
                            echo "title=\"${title}\"" >"${1}/lasttitle/${2}.title"
                        else
                            if [ "${line_2##a?href*e?hentai?org?g?[0-9]*}" == "" ]
                            then
                                target_raw=${line_2:8:64}
                                target=${target_raw%??}
                                source "${1}/lasttitle/${2}.title"
                                rm "${1}/lasttitle/${2}.title"
                                echo -n "filename=" >"${1}/filename/${2}.tmp"
                                echo -n "${target}"|md5sum|cut -d ' ' -f 1 >>"${1}/filename/${2}.tmp"
                                source "${1}/filename/${2}.tmp"
                                rm "${1}/filename/${2}.tmp"
                                #!对title进行处理，防止文件夹名出错
                                title=${title//\//_}
                                title=${title:0:230}
                                echo "target=\"${target}\"" >${index_directory}/${filename}.index
                                echo "title=\"${title}\"" >>${index_directory}/${filename}.index
                            fi
                        fi
                    fi
                done
            fi
        fi
    fi
done
rm "${1}/searchpage/${2}.tmp"
exit