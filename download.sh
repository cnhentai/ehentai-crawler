#!/bin/bash
source setting.cfg
source "${2}"
if [ -f "${1}/stop.eh" ]
then
    warn_banned="false"
else
    warn_banned="true"
fi
grep -F "${title}" ${download_db}>/dev/null && exit
curl --retry 5 --retry-connrefused --tcp-fastopen -s "${target}" >"${1}/galleryentry/${title}.tmp"
if [ ! -s "${1}/galleryentry/${title}.tmp" ]
then
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            echo "Unable to download Gallery Entry. You might have been banned."
        fi
    fi
    exit
fi
grep "Your IP address has been temporarily banned for" "${1}/galleryentry/${title}.tmp" && (
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            echo "You have been banned."
        fi
    fi
    exit
)
mkdir "${1}/saved_comic/${title}"
cat "${1}/galleryentry/${title}.tmp" | while read line
do
    if [ "${line##*Report?Gallery*}" == "" ]
    then
        echo "${line}" |xargs -d '<' -n 1 | while read line_2
        do
            if [ "${line_2##a?href*e?hentai?org?s\/*}" == "" ]
            then
                target_url_dirty="${line_2##a?href??}"
                target_url="${target_url_dirty%??}"
                curl --retry 5 --retry-connrefused --tcp-fastopen -s "${target_url}" >"${1}/bigpics/${title}.tmp"
                if [ ! -f "${1}/bigpics/${title}.tmp" ]
                then
                    if ${warn_banned}
                    then
                        if [ ! -f "${1}/stop.eh" ]
                        then
                            echo "Unable to download Gallery Entry. You might have been banned."
                        fi
                    fi
                    exit
                fi
                grep "Your IP address has been temporarily banned for" "${1}/bigpics/${title}.tmp" && (
                    if ${warn_banned}
                    then
                        if [ ! -f "${1}/stop.eh" ]
                        then
                            echo "Unable to download Gallery Entry. You might have been banned."
                        fi
                    fi
                    exit
                )
                cat "${1}/bigpics/${title}.tmp" | while read line_3
                do
                    if [ "${line_3##*return?load?image*}" == "" ]
                    then
                        echo "${line_3}" |xargs -d '<' -n 1 | while read line_4
                        do
                            if [ "${line_4}" != "" ]
                            then
                                if [ "${line_4##img*style*width*px*}" == "" ]
                                then
                                    url_dirty="${line_4#img?id??img??src??}"
                                    url="${url_dirty%??style*width*}"
                                    unset url_dirty
                                    curl --retry 5 --retry-connrefused --tcp-fastopen -s -o "${1}/saved_comic/${title}/${target_url##*-}.jpg" "${url}" 
                                fi
                            fi
                        done
                    fi
                done
                rm "${1}/bigpics/${title}.tmp"
            fi
        done
    fi
done
rm "${1}/galleryentry/${title}.tmp"
if [ "`ls -A "${1}/saved_comic/${title}/"`" == "" ]
then
    rm -r "${1}/saved_comic/${title}"
else
    (
        mv -u "${1}/saved_comic/${title}" "${download_directory}/${title}"
    ) &
    (
        echo "${title}" >>${download_db}
    ) &
    wait
    echo "Downloaded ${title}"
fi
exit